﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_EventDrivePrograms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnHello_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hello");
        }

        private void TxtMyWords_TextChanged(object sender, TextChangedEventArgs e)
        {
            txbWelcome.Text = txtMyWords.Text.Length.ToString();
        }

        private void LstFruit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBox.Show($"You like {lstFruit.SelectedItem.ToString()}");//nem jó
        }

        private void TxbWelcome_MouseEnter(object sender, MouseEventArgs e)
        {
            txbWelcome.Background = Brushes.Red;
        }
    }
}
